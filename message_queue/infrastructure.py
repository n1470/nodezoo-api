"""
Messaging infrastructure
"""
from aws_cdk import aws_sqs
from aws_cdk import CfnOutput
from constructs import Construct


class QueueBuilder(Construct):
    """
    Message queues on AWS
    """
    def __init__(self, scope: Construct, id_: str, *args, **kwargs) -> None:
        super().__init__(scope, id_, *args, **kwargs)

        self._queue_url_var_name = f"{id_}_url"
        self._queue = aws_sqs.Queue(scope=self,
                                    id=f"{id_}_queue",
                                    encryption=aws_sqs.QueueEncryption.KMS)
        CfnOutput(scope=self,
                  id=self._queue_url_var_name,
                  value=self._queue.queue_url,
                  export_name=f"nodezoo-sqs-{self._queue_url_var_name.replace('_', '-')}")

    @property
    def queue(self):
        """
        Queue construct
        :return:
        """
        return self._queue

    @property
    def queue_url_var_name(self):
        """
        Queue URL variable name. Use this name to import the name in another
        stack.
        :return:
        """
        return self._queue_url_var_name

    def add_send_permissions(self, aws_lambda_fn):
        """
        Permit the Lambda to send messages to this queue.
        :param aws_lambda_fn:
        :return:
        """
        self._queue.grant_send_messages(aws_lambda_fn)

    def add_receive_permissions(self, aws_lambda_fn):
        """
        Permit the Lambda to receive messages from this queue.
        :param aws_lambda_fn:
        :return:
        """
        self._queue.grant_consume_messages(aws_lambda_fn)

    @property
    def queue_url(self):
        """
        AWS queue (SQS) URL.
        :return:
        """
        return self._queue.queue_url
