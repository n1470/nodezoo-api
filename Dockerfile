FROM python:3.9.10

ARG PROJECT_PATH

RUN apt-get update && \
    apt-get -y upgrade && \
    curl -fsSL https://deb.nodesource.com/setup_16.x | bash - && \
    apt-get install -y nodejs zip && \
    npm install -g aws-cdk

RUN curl -sSL https://install.python-poetry.org | python3 -

ENV PATH="/root/.local/bin:$PATH"

WORKDIR /builds/$PROJECT_PATH

COPY pyproject.toml .
COPY poetry.lock .

RUN poetry install

ENV PYTHONPATH=$PYTHONPATH:/builds/$PROJECT_PATH/api/runtime:/builds/$PROJECT_PATH/pkg_info_gateway/runtime

CMD ["bash"]
