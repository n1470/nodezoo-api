"""
CDK application
"""
import argparse

import aws_cdk as cdk

import lambda_config
from deployment import NodezooApiStack

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--profile")
parser.add_argument("--info-api-url")

args_ = parser.parse_args()
kwargs = dict(
    StackProps=dict()
)
if args_.info_api_url is None:
    lambda_config.add_attributes(attrs=kwargs["StackProps"], profile=args_.profile)
else:
    kwargs["StackProps"]["info_api_url"] = args_.info_api_url

app = cdk.App()
NodezooApiStack(app, "nodezoo", **kwargs)
app.synth()
