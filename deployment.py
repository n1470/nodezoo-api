"""
CDK deployment
"""
import typing

import constructs
from aws_cdk import Stack
from api.infrastructure import ApiBuilder
from message_queue.infrastructure import QueueBuilder


class NodezooApiStack(Stack):
    """
    Independently deploy API
    """
    def __init__(self,
                 scope: typing.Optional[constructs.Construct],
                 id_: typing.Optional[str],
                 *args,
                 **kwargs) -> None:
        super().__init__(scope, id_,
                         *args,
                         **self._parent_kwargs(kwargs))

        def _destined_for(role: str, message_type: str, value: str):
            return f"{role}-{message_type}-{value}"

        info_queue_id = _destined_for(role="info",
                                      message_type="cmd",
                                      value="get")
        info_reply_queue_id = _destined_for(role="info",
                                            message_type="cmd",
                                            value="get-reply")
        search_queue_id = _destined_for(role="search",
                                        message_type="cmd",
                                        value="search")
        search_reply_queue_id = _destined_for(role="search",
                                              message_type="cmd",
                                              value="search-reply")
        info_queue = QueueBuilder(self, info_queue_id)
        info_reply_queue = QueueBuilder(self, info_reply_queue_id)
        search_queue = QueueBuilder(self, search_queue_id)
        search_reply_queue = QueueBuilder(self, search_reply_queue_id)
        kwargs_ = dict(info_queue_builder=info_queue,
                       info_reply_queue_builder=info_reply_queue,
                       search_queue_builder=search_queue,
                       search_reply_queue_builder=search_reply_queue,
                       **kwargs["StackProps"])
        # runtime_dist_dir = kwargs["StackProps"].get('runtime_dist_dir')
        # if runtime_dist_dir is not None:
        #     kwargs_['runtime_dist_dir'] = runtime_dist_dir
        ApiBuilder(self, "api", **kwargs_)

    @staticmethod
    def _parent_kwargs(kwargs):
        return {k: v for k, v in kwargs.items() if k != "StackProps"}
