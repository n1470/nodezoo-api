"""
CDK for SQS triggered Lambda
"""
import os
from typing import Sequence

import aws_cdk
import aws_cdk.aws_ec2 as ec2
from aws_cdk import aws_lambda, Duration
from aws_cdk.aws_iam import IRole, Policy, PolicyStatement, Effect
from aws_cdk.aws_lambda import Tracing
from constructs import Construct


def build_trigger_runtime(construct: Construct,
                          **kwargs) -> aws_lambda.Function:
    """
    Defines a Lambda with access to VPC private subnet(s).
    """
    runtime_dist_dir = kwargs.get('runtime_dist_dir', "runtime_dist")
    lambda_env = kwargs.get('env_params', {})
    lambda_env['nodezoo_info_api_url'] = \
        kwargs["info_api_url"] if "info_api_url" in kwargs else f"http://{kwargs['info_alb_host']}"
    runtime_root = os.path.dirname(__file__)
    dist_path = os.path.join(runtime_root, runtime_dist_dir)

    vpc_ = None
    sec_gru_ = None
    if "info_api_url" not in kwargs:
        vpc_ = ec2.Vpc.from_vpc_attributes(construct,
                                           "nodezoo-eks-vpc",
                                           vpc_id=kwargs["vpc_id"],
                                           availability_zones=aws_cdk.Fn.get_azs(),
                                           private_subnet_ids=kwargs["vpc_pvt_subnet_ids"])
        sec_gru_ = [
            ec2.SecurityGroup.from_security_group_id(
                construct,
                "info-alb-http",
                kwargs["security_group_id"]
            )
        ]

    function = aws_lambda.Function(construct, "info_trigger",
                                   runtime=aws_lambda.Runtime.PYTHON_3_9,
                                   handler="queue_lambda.handle_message",
                                   code=aws_lambda.Code.from_asset(dist_path),
                                   environment=lambda_env,
                                   timeout=Duration.seconds(30),
                                   vpc=vpc_,
                                   security_groups=sec_gru_,
                                   tracing=Tracing.ACTIVE)

    if "info_api_url" not in kwargs:
        _attach_vpc_access_policy(construct,
                                  function.role,
                                  region=kwargs["region"],
                                  account_id=kwargs["account_id"],
                                  vpc_id=kwargs["vpc_id"])

    return function


def _attach_vpc_access_policy(construct: Construct,
                              function_role: IRole,
                              region: str,
                              account_id: str,
                              vpc_id: str):
    policy_statements: Sequence[PolicyStatement] = [
        PolicyStatement(
            sid="InfoTrigger0",
            effect=Effect.ALLOW,
            actions=[
                "ec2:CreateNetworkInterface",
                "ec2:CreateTags",
                "ec2:DeleteNetworkInterface"
            ],
            resources=[
                f"arn:aws:ec2:{region}:{account_id}:vpc/{vpc_id}",
                f"arn:aws:ec2:*:{account_id}:network-interface/*"
            ]
        ),
        PolicyStatement(
            sid="InfoTrigger1",
            effect=Effect.ALLOW,
            actions=[
                "ec2:DescribeNetworkInterfaces"
            ],
            resources=[
                "*"
            ]
        )
    ]
    function_role.attach_inline_policy(Policy(
        scope=construct,
        id="info-alb-access",
        policy_name="TriggerVpcAccessPolicy",
        statements=policy_statements
    ))
