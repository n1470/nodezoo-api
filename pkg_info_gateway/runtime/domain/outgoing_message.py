"""
Domain object: outgoing message to queue
"""
from dataclasses import dataclass


@dataclass(frozen=True)
class OutgoingMessage:
    """
    outgoing SQS message body
    """
    correlation_id: str
    body: str
    title: str
