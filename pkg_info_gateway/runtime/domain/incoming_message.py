"""
Domain object: incoming SQS message body
"""
from dataclasses import dataclass


@dataclass(frozen=True)
class IncomingMessage:
    """
    Incoming SQS Message Body
    """
    correlation_id: str
    package_query: str
