"""
AWS Lambda proxy for API Gateway
"""
from __future__ import annotations

import base64
import gzip
from dataclasses import asdict, dataclass
import http.client
import json
import logging
import os
from urllib.parse import urlparse

import boto3

from domain.incoming_message import IncomingMessage
from domain.outgoing_message import OutgoingMessage

HTTP_DEFAULT_PORT = 80
HTTPS_DEFAULT_PORT = 443
SCHEME_PORT_MAP = {'https': HTTPS_DEFAULT_PORT}

log_level = os.environ.get("LOG_LEVEL", logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(log_level)


def _fetch_package_info(info_api_url: str, package_query: str) -> str:
    logger.debug(f"info_api_url = {info_api_url}")
    parse_result = urlparse(info_api_url)
    scheme, host, maybe_port = parse_result.scheme, parse_result.hostname, parse_result.port
    port = maybe_port if maybe_port is not None else SCHEME_PORT_MAP.get(scheme, HTTP_DEFAULT_PORT)
    http_conn = http.client.HTTPSConnection(host, port) if scheme == 'https' else http.client.HTTPConnection(host, port)
    http_conn.request("GET", url=f"{info_api_url}/info?q={package_query}")
    response = http_conn.getresponse()
    if response.status == 200:
        return str(response.read(), 'UTF-8')
    if response.status == 404:
        return '{}'
    raise RuntimeError(f"Info service returned error: {response.status}")


def _post_message(info_reply_sqs_url: str,
                  aws_region_code: str,
                  outgoing: OutgoingMessage) -> str:
    sqs = boto3.resource('sqs', region_name=aws_region_code)
    queue = sqs.Queue(url=info_reply_sqs_url)

    outgoing_json = json.dumps(asdict(outgoing))
    outgoing_compressed_json = gzip.compress(outgoing_json.encode("UTF-8"))
    sqs_payload = base64.urlsafe_b64encode(outgoing_compressed_json).decode("UTF-8")
    sqs_resp = queue.send_message(MessageBody=sqs_payload)
    return sqs_resp.get('MessageId')


@dataclass(frozen=True)
class LambdaVars:
    """
    Environment variables for info-trigger Lambda
    """
    info_api_url: str
    info_reply_sqs_url: str
    aws_region_code: str


def _handle_request(source: str,
                    lambda_vars: LambdaVars,
                    fetch_package_info,
                    post_message) -> (str | None):
    source_obj = json.loads(source)
    incoming = IncomingMessage(correlation_id=source_obj['correlation_id'],
                               package_query=source_obj['body']['get'])
    try:
        package_info = fetch_package_info(lambda_vars.info_api_url,
                                          package_query=incoming.package_query)
        if package_info != '{}':
            outgoing = OutgoingMessage(correlation_id=incoming.correlation_id,
                                       body=package_info,
                                       title='info')
            return post_message(lambda_vars.info_reply_sqs_url,
                                lambda_vars.aws_region_code,
                                outgoing)
    except RuntimeError as runtime_error:
        logger.exception(runtime_error)
    return None


def handle_message(event: dict, _context,
                   fetch_package_info=_fetch_package_info,
                   post_message=_post_message):
    """
    AWS Lambda handler for messages from sqs.
    :param post_message:
    :type fetch_package_info: (str, str) -> str
    :param fetch_package_info:
    :param event:
    :param _context: LambdaContext
    :return:
    """
    logger.debug(json.dumps(event))
    lambda_vars = LambdaVars(info_api_url=os.environ.get('nodezoo_info_api_url', "http://localhost:8080"),
                             info_reply_sqs_url=os.environ.get('nodezoo_info_reply_queue_url',
                                                               "https://sqs.aws.local/info-get-reply"),
                             aws_region_code=os.environ.get('AWS_REGION', 'ap-south-1'))
    responses_with_none = [_handle_request(message['body'],
                                           lambda_vars,
                                           fetch_package_info,
                                           post_message) for message in event['Records']]
    return [item for item in responses_with_none if item is not None]
