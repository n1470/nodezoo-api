import pytest
import aws_cdk as cdk
from aws_cdk import assertions
from deployment import NodezooApiStack


@pytest.mark.skip
def test_stack_synth():
    app = cdk.App()
    stack = NodezooApiStack(app, "nodezoo-test",
                            info_api_url="http://localhost:9000",
                            runtime_dist_dir="runtime")
    template = assertions.Template.from_stack(stack)
    template.has_resource("AWS::SQS::Queue", 4)
    template.has_resource("AWS::Lambda::Function", 2)
    template.has_resource("AWS::ApiGateway::RestApi", 1)
