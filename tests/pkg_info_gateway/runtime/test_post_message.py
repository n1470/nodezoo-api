import json

import boto3
import os
import pytest
import pytest_localstack

from domain.outgoing_message import OutgoingMessage
from queue_lambda import _post_message

localstack = pytest_localstack.patch_fixture(
    services=["sqs"],
    scope="module",
    autouse=True
)


@pytest.mark.skipif(condition=os.environ.get("CI") is not None, reason="No CI license")
def test_post_message():
    aws_region = "us-east-1"
    sqs = boto3.resource("sqs", region_name=aws_region)
    queue = sqs.create_queue(QueueName="NodezooInfoReply")
    outgoing = OutgoingMessage(correlation_id="r123", title="info", body=json.dumps(dict(id=1, name="react")))
    reply_id = _post_message(info_reply_sqs_url=queue.url, aws_region_code=aws_region, outgoing=outgoing)
    assert reply_id is not None
