import json
from pytest_httpserver import HTTPServer

from queue_lambda import _fetch_package_info


def test_fetch_package_info(httpserver: HTTPServer):
    canned_api_response = dict(id=1, name="express")
    httpserver.expect_request("/info",
                              query_string="q=express",
                              method="GET").respond_with_json(canned_api_response)
    resp: str = _fetch_package_info(info_api_url=httpserver.url_for("/"),
                                    package_query="express")
    assert json.loads(resp) == canned_api_response


def test_fetch_package_info_not_found(httpserver: HTTPServer):
    httpserver.expect_request("/info",
                              query_string="q=jujuba",
                              method="GET").respond_with_json(dict(), status=404)
    resp: str = _fetch_package_info(info_api_url=httpserver.url_for("/"),
                                    package_query="jujuba")
    assert json.loads(resp) == dict()
