import json

from queue_lambda import handle_message


def test_single_message_package_found(mocker):
    """
    When package is found, package info is posted to the queue.
    """
    _fetch_pkg_info = mocker.Mock(return_value=json.dumps(dict(id=1, name="react")))
    _post_message = mocker.Mock(return_value="r123")
    event = dict(Records=[
        dict(body=json.dumps(dict(correlation_id="x123", body=dict(get="react"))))
    ])
    reply_ids = handle_message(event, dict(), _fetch_pkg_info, _post_message)
    assert reply_ids is not None
    assert reply_ids == ["r123"]


def test_single_message_package_not_found(mocker):
    """
    When package is not found, package info is not posted to the queue
    """
    _fetch_pkg_info = mocker.Mock(return_value=json.dumps(dict()))
    _post_message = mocker.Mock()
    event = dict(Records=[
        dict(body=json.dumps(dict(correlation_id="x123", body=dict(get="rails"))))
    ])
    reply_ids = handle_message(event, dict(), _fetch_pkg_info, _post_message)
    assert reply_ids is not None
    assert reply_ids == []
    _post_message.assert_not_called()


def test_multiple_messages_no_runtime_errors_all_found(mocker):
    """
    When multiple packages are found without runtime errors, multiple message receipts
    are returned.
    """
    canned_responses = dict(react=dict(pkg_info=dict(id=1, name="react"), reply_id="r123"),
                            lodash=dict(pkg_info=dict(id=2, name="lodash"), reply_id="r456"))

    def fetch_pkg_info_(_url, package_query):
        return json.dumps(canned_responses[package_query]['pkg_info'])

    _fetch_pkg_info = mocker.Mock(side_effect=fetch_pkg_info_)
    _post_message = mocker.Mock(side_effect=(cr['reply_id'] for cr in canned_responses.values()))
    event = dict(Records=[
            dict(body=json.dumps(dict(correlation_id="x123", body=dict(get="react")))),
            dict(body=json.dumps(dict(correlation_id="x123", body=dict(get="lodash"))))
        ])
    reply_ids = handle_message(event, dict(), _fetch_pkg_info, _post_message)
    assert reply_ids is not None
    assert reply_ids == ["r123", "r456"]


def test_multiple_messages_no_runtime_errors_some_found(mocker):
    """
    Receipt handles for found packages are returned.
    """
    canned_responses = dict(react=dict(id=1, name="react"), rails=dict())

    def fetch_pkg_info_(_url, package_query):
        return json.dumps(canned_responses[package_query])

    _fetch_pkg_info = mocker.Mock(side_effect=fetch_pkg_info_)
    _post_message = mocker.Mock(return_value="r123")
    event = dict(Records=[
            dict(body=json.dumps(dict(correlation_id="x123", body=dict(get="react")))),
            dict(body=json.dumps(dict(correlation_id="x324", body=dict(get="rails"))))
        ])
    reply_ids = handle_message(event, dict(), _fetch_pkg_info, _post_message)
    assert reply_ids is not None
    assert reply_ids == ["r123"]
    _post_message.assert_called_once()


def test_multiple_messages_with_some_runtime_error(mocker):
    """
    Receipt handles for packages with no runtime errors are returned.
    """
    def fetch_pkg_info_(_url, package_query):
        if package_query == 'react':
            return json.dumps(dict(id=1, name="react"))
        raise RuntimeError("mock error")

    _fetch_pkg_info = mocker.Mock(side_effect=fetch_pkg_info_)
    _post_message = mocker.Mock(return_value="r123")
    event = dict(Records=[
            dict(body=json.dumps(dict(correlation_id="x123", body=dict(get="react")))),
            dict(body=json.dumps(dict(correlation_id="x324", body=dict(get="lodash"))))
        ])
    reply_ids = handle_message(event, dict(), _fetch_pkg_info, _post_message)
    assert reply_ids is not None
    assert reply_ids == ["r123"]
    _post_message.assert_called_once()
