from pytest_mock import MockerFixture

from nodezoo_api.core.abstract_event_handler import AbstractEventHandler
from nodezoo_api.handler_factory import HandlerFactory
from nodezoo_api import app
from nodezoo_api.core.app_event import AppEvent
from nodezoo_api.core.queue_set import QueueSet


def test_process_event(mocker: MockerFixture):
    event_handler = mocker.Mock(spec_set=AbstractEventHandler)
    handler_factory = mocker.Mock(spec_set=HandlerFactory)
    handler_factory.create = mocker.Mock(return_value=event_handler)
    queues = QueueSet(info_queue_url='a',
                      info_queue_reply_url='ar',
                      search_queue_url='s',
                      search_queue_reply_url='sr')
    app_event = AppEvent(request_id='req-123',
                         correlation_id='x-123',
                         request_path='/info',
                         aws_region_code='ap-south-1',
                         request_params=dict(package='express'),
                         queues=queues)
    app.process_event(app_event=app_event, handler_factory=handler_factory)
    event_handler.handle_event.assert_called_once()
    event_handler.fetch_response.assert_called_once()
