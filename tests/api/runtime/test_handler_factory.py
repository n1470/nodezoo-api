import pytest
from pytest_mock import MockerFixture

from nodezoo_api.core.app_event import AppEvent
from nodezoo_api.core.queue_set import QueueSet
from nodezoo_api.core.nodezoo_error import NodezooNotFoundError
from nodezoo_api.handler.info_event_handler import InfoEventHandler
from nodezoo_api.handler.search_event_handler import SearchEventHandler
from nodezoo_api.handler_factory import HandlerFactory
from nodezoo_api.core.queue_adapter import QueueAdapter


@pytest.fixture
def handler_factory(mocker: MockerFixture):
    info_queue_adapter = mocker.Mock(spec_set=QueueAdapter)
    search_queue_adapter = mocker.Mock(spec_set=QueueAdapter)
    return HandlerFactory(info_queue_adapter=info_queue_adapter,
                          search_queue_adapter=search_queue_adapter)


@pytest.fixture
def queues() -> QueueSet:
    return QueueSet(info_queue_url='i',
                    info_queue_reply_url='ir',
                    search_queue_url='s',
                    search_queue_reply_url='sr')


def test_create_info_handler(handler_factory, queues):
    app_event = AppEvent(request_id='req-123',
                         correlation_id='x-123',
                         request_path='/info',
                         aws_region_code='ap-south-1',
                         request_params=dict(package='express'),
                         queues=queues)
    handler = handler_factory.create(app_event)
    assert handler.__class__ == InfoEventHandler
    assert handler.queue_adapter == handler_factory.info_queue_adapter


def test_create_search_handler(handler_factory, queues):
    app_event = AppEvent(request_id='req-123',
                         correlation_id='x-123',
                         request_path='/search',
                         aws_region_code='ap-south-1',
                         request_params=dict(q='express'),
                         queues=queues)
    handler = handler_factory.create(app_event)
    assert handler.__class__ == SearchEventHandler
    assert handler.queue_adapter == handler_factory.search_queue_adapter


def test_unknown_request_path(handler_factory, queues):
    app_event = AppEvent(request_id='req-123',
                         correlation_id='x-123',
                         request_path='/foo',
                         aws_region_code='ap-south-1',
                         queues=queues)
    with pytest.raises(NodezooNotFoundError):
        handler_factory.create(app_event)
