import pytest
import json
from pytest_mock import MockerFixture
from string import Template

from nodezoo_api.core.queue_set import QueueSet
from nodezoo_api.core.app_event import AppEvent
from nodezoo_api.core.nodezoo_error import NodezooNotFoundError
from nodezoo_api.aws import lambda_adapter


def api_gateway_event(path: str):
    event_template = Template("""
      {
        "version":"2.0",
          "routeKey":"GET /$path",
          "path":"/$path",
          "rawQueryString":"q=express",
          "headers":{
            "accept":"*/*",
            "content-length":"0",
            "host":"nx28k0.execute-api.ap-south-1.amazonaws.com",
            "user-agent":"curl/7.74.0",
            "x-amzn-trace-id":"Root=1-61cca7ca-1b23d8b814d96ca157f80d5a",
            "x-forwarded-for":"12.16.49.10",
            "x-forwarded-port":"443",
            "x-forwarded-proto":"https"
          },
          "queryStringParameters":{"q":"express"},
          "requestContext":{
            "accountId":"123",
            "apiId":"nx28k0",
            "domainName":"nx28k0.execute-api.ap-south-1.amazonaws.com",
            "domainPrefix":"nx28k0",
            "http":{
              "method":"GET",
              "path":"/$path",
              "protocol":"HTTP/1.1",
              "sourceIp":"12.16.49.10",
              "userAgent":"curl/7.74.0"
            },
            "requestId":"LH8nthFSBcwEMrw=",
            "routeKey":"GET /$path",
            "stage":"$default",
            "time":"29/Dec/2021:18:24:10 +0000",
            "timeEpoch":1640802250747
          },
          "isBase64Encoded":false,
          "pathParameters":{"package": "express"}
        }
      """)
    return json.loads(event_template.safe_substitute(path=path))


@pytest.fixture
def lambda_context(mocker: MockerFixture):
    return mocker.Mock(aws_request_id="LH8nthFSBcwEMrw=")


@pytest.fixture
def lambda_env():
    return dict(_X_AMZN_TRACE_ID="Root=1-61cca7ca-1b23d8b814d96ca157f80d5a",
                AWS_REGION="ap-south-1",
                nodezoo_info_queue_url="sqs://role-info-cmd-get",
                nodezoo_info_reply_queue_url="sqs://role-info-cmd-get-reply",
                nodezoo_search_queue_url="sqs://role-search-cmd-search",
                nodezoo_search_reply_queue_url="sqs://role-search-cmd-search-reply")


def test_event_processing(mocker: MockerFixture, lambda_context, lambda_env):
    mock_process_event = mocker.Mock(name="mock_process_event", return_value=[])
    lambda_adapter.handle(event=api_gateway_event(path="search"),
                          context=lambda_context,
                          process_event=mock_process_event,
                          proxy_lambda_env=lambda_env)
    mock_process_event.assert_called_once()
    process_event_args: dict = mock_process_event.call_args.kwargs
    app_event: AppEvent = process_event_args['app_event']
    assert app_event.request_id == lambda_context.aws_request_id
    assert app_event.correlation_id == "Root=1-61cca7ca-1b23d8b814d96ca157f80d5a"
    assert app_event.aws_region_code == "ap-south-1"
    assert app_event.request_path == "/search"
    assert app_event.request_params == dict(q="express", package="express")
    assert app_event.queues == QueueSet(info_queue_url="sqs://role-info-cmd-get",
                                        info_queue_reply_url="sqs://role-info-cmd-get-reply",
                                        search_queue_url="sqs://role-search-cmd-search",
                                        search_queue_reply_url="sqs://role-search-cmd-search-reply")


def test_unknown_event(mocker: MockerFixture, lambda_context, lambda_env):
    mock_process_event = mocker.Mock(name="mock_process_event", side_effect=NodezooNotFoundError())
    response = lambda_adapter.handle(event=api_gateway_event(path="fu"),
                                     context=lambda_context,
                                     process_event=mock_process_event,
                                     proxy_lambda_env=lambda_env)
    assert response.__class__ == dict
    assert response["statusCode"] == 404
