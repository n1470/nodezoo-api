import dataclasses
import os

import pytest
import json
import re
from mypy_boto3_sqs.client import SQSClient
from pytest_mock import MockerFixture

from nodezoo_api.aws.sqs_adapter import SqsAdapter
from nodezoo_api.domain.message import Message
from nodezoo_api.core.nodezoo_error import NodezooError


def test_post_message(mocker: MockerFixture):
    mock_sqs_client = mocker.Mock(name='sqs_client', spec_set=SQSClient)
    mock_sqs_client.send_message.return_value = {'MessageId': '123'}
    sqs_adapter = SqsAdapter(aws_region_code='us-east-1',
                             send_queue_url='https://sqs.us-east-2.amazonaws.com/123456789012/MyQueue',
                             receive_queue_url='sr',
                             sqs_client=mock_sqs_client)
    message = Message(event_id='e123', correlation_id='x123', body={'x': 1, 'y': 2}, dest='MyQueue')
    message_id = sqs_adapter.post_message(queue_name='MyQueue', message=message)
    assert message_id is not None
    send_message_args = mock_sqs_client.send_message.call_args.kwargs
    assert send_message_args['QueueUrl'] == "https://sqs.us-east-2.amazonaws.com/123456789012/MyQueue"
    payload = json.loads(send_message_args['MessageBody'])
    assert payload['body'] == {"x": 1, "y": 2}


def test_post_message_destination_mismatch(mocker: MockerFixture):
    mock_sqs_client = mocker.Mock(name='sqs_client', spec_set=SQSClient)
    sqs_adapter = SqsAdapter(aws_region_code='us-east-1',
                             send_queue_url='s',
                             receive_queue_url='sr',
                             sqs_client=mock_sqs_client)
    message = Message(event_id='e123', correlation_id='x123', body={'x': 1, 'y': 2}, dest='MyQueue')
    with pytest.raises(NodezooError) as nodezoo_error:
        sqs_adapter.post_message(queue_name='OtherQueue', message=message)
    assert re.match('destination mismatch', str(nodezoo_error.value), re.IGNORECASE)


@pytest.mark.skipif(condition=os.environ.get("CI") is not None, reason="known issue")
def test_receive_message(mocker: MockerFixture):
    mock_sqs_client = mocker.Mock(name='sqs_client', spec_set=SQSClient)
    payload_message = Message(event_id='m123',
                              correlation_id='c1',
                              body=dict(x=1, y=1),
                              title='something')
    mock_sqs_client.receive_message.return_value = dict(Messages=[
        dict(Body=json.dumps(dataclasses.asdict(payload_message)),
             MessageId='m123',
             ReceiptHandle='r123')
    ])
    sqs_adapter = SqsAdapter(aws_region_code='us-east-1',
                             send_queue_url='s',
                             receive_queue_url='sr',
                             sqs_client=mock_sqs_client)
    received_messages = [message for message in sqs_adapter.receive_message()]
    assert len(received_messages) > 0
    for message in received_messages:
        assert message is not None
        assert message.event_id == 'm123'
        assert message.correlation_id == 'c1'
        assert message.body == payload_message.body
        assert message.receipt_handle == 'r123'


def test_acknowledge_message(mocker: MockerFixture):
    mock_sqs_client = mocker.Mock(name='sqs_client', spec_set=SQSClient)
    sqs_adapter = SqsAdapter(aws_region_code='us-east-1',
                             send_queue_url='s',
                             receive_queue_url='sr',
                             sqs_client=mock_sqs_client)
    sqs_adapter.acknowledge_message(message_handle='r123')
    mock_sqs_client.delete_message.assert_called_once_with(**dict(QueueUrl='sr', ReceiptHandle='r123'))
