from pytest_mock import MockerFixture

from nodezoo_api.core.queue_adapter import QueueAdapter
from nodezoo_api.domain.info_api_event import InfoApiEvent
from nodezoo_api.domain.message import Message
from nodezoo_api.handler.info_event_handler import InfoEventHandler


def test_fetch_response_multiple_tries(mocker: MockerFixture):
    event = InfoApiEvent(
        event_id='evt-123',
        correlation_id='x123',
        resource='/info/get',
        data='express'
    )

    reply_message = Message(
        correlation_id=event.correlation_id,
        event_id='evt-789',
        body=dict(x=1, y=2, z=3),
        title='evt-456'
    )

    mock_adapter = mocker.Mock(spec_set=QueueAdapter)
    mock_adapter.receive_message = mocker.Mock(side_effect=[iter([]), iter([]), iter([reply_message])])
    handler = InfoEventHandler(event=event, queue_adapter=mock_adapter)
    handler.doze_factor = 0
    queued_message = handler.handle_event()
    resp = handler.fetch_response(queued_message)
    assert resp == reply_message.body
    mock_adapter.receive_message.assert_called()
