import pytest

from pytest_mock import MockerFixture

from nodezoo_api.core.nodezoo_error import NodezooNotFoundError
from nodezoo_api.domain.info_api_event import InfoApiEvent
from nodezoo_api.domain.message import Message
from nodezoo_api.core.queue_adapter import QueueAdapter
from nodezoo_api.handler.info_event_handler import InfoEventHandler


def test_info_event_handling(mocker: MockerFixture):
    event = InfoApiEvent(
        event_id='evt-123',
        correlation_id='x123',
        resource='/info/get',
        data='express'
    )

    mock_adapter = mocker.Mock(spec_set=QueueAdapter)
    handler = InfoEventHandler(event=event, queue_adapter=mock_adapter)
    message = handler.handle_event()
    mock_adapter.post_message.assert_called_once()
    post_message_args = mock_adapter.post_message.call_args.kwargs
    assert post_message_args['queue_name'] == 'info-cmd-get'
    assert message == post_message_args['message']
    assert message.reply_to == 'reply-info-cmd-get'
    assert message.body == dict(role="info", type="cmd", cmd="get", get="express")


def test_fetch_response(mocker: MockerFixture):
    event = InfoApiEvent(
        event_id='evt-123',
        correlation_id='x123',
        resource='/info/get',
        data='express'
    )

    reply_message = Message(
        correlation_id=event.correlation_id,
        event_id='evt-789',
        body=dict(x=1, y=2, z=3),
        title='evt-456'
    )

    mock_adapter = mocker.Mock(spec_set=QueueAdapter)
    mock_adapter.receive_message = mocker.Mock(return_value=iter([reply_message]))
    handler = InfoEventHandler(event=event, queue_adapter=mock_adapter)
    message = handler.handle_event()
    resp = handler.fetch_response(message)
    assert resp == reply_message.body
    mock_adapter.receive_message.assert_called_once()


def test_fetch_response_no_result(mocker: MockerFixture):
    event = InfoApiEvent(
        event_id='evt-123',
        correlation_id='x123',
        resource='/info/get',
        data='express'
    )

    mock_adapter = mocker.Mock(spec_set=QueueAdapter)
    mock_adapter.receive_message = mocker.Mock(return_value=iter([]))
    handler = InfoEventHandler(event=event, queue_adapter=mock_adapter)
    handler.doze_factor = 0
    message = handler.handle_event()
    with pytest.raises(NodezooNotFoundError):
        handler.fetch_response(message)


def test_fetch_response_ignore_unmatched_message(mocker: MockerFixture):
    event = InfoApiEvent(
        event_id='evt-123',
        correlation_id='x123',
        resource='/info/get',
        data='express'
    )

    reply_message = Message(
        correlation_id=event.correlation_id,
        event_id='evt-789',
        body=dict(x=1, y=2, z=3),
        title='evt-456'
    )

    other_reply_message = Message(
        correlation_id='x456',
        event_id='evt-789',
        body=dict(x=1, y=2, z=3),
        title='evt-456'
    )

    mock_adapter = mocker.Mock(spec_set=QueueAdapter)
    mock_adapter.receive_message = mocker.Mock(side_effect=[iter([other_reply_message]), iter([reply_message])])
    handler = InfoEventHandler(event=event, queue_adapter=mock_adapter)
    handler.doze_factor = 0
    message = handler.handle_event()
    resp = handler.fetch_response(message)
    assert resp == reply_message.body
