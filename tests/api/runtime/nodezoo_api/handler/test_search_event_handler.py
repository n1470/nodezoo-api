from pytest_mock.plugin import MockerFixture
from nodezoo_api.domain.search_api_event import SearchApiEvent
from nodezoo_api.core.queue_adapter import QueueAdapter
from nodezoo_api.handler.search_event_handler import SearchEventHandler
from nodezoo_api.domain.message import Message


def test_search_event_handling(mocker: MockerFixture):
    event = SearchApiEvent(
        event_id='evt-123',
        correlation_id='x123',
        query='express'
    )

    mock_adapter = mocker.Mock(spec_set=QueueAdapter)
    handler = SearchEventHandler(event=event, queue_adapter=mock_adapter)
    message: Message = handler.handle_event()
    mock_adapter.post_message.assert_called_once()
    post_message_args = mock_adapter.post_message.call_args.kwargs
    assert post_message_args['queue_name'] == 'search-cmd-search'
    assert message == post_message_args['message']
    assert message.reply_to == 'reply-search-cmd-search'
    assert message.body == dict(role="search", type="cmd", cmd="search", query="express")


def test_fetch_response(mocker: MockerFixture):
    event = SearchApiEvent(
        event_id='evt-123',
        correlation_id='x123',
        query='express'
    )

    reply_message = Message(
        correlation_id=event.correlation_id,
        event_id='evt-789',
        body=(dict(x=1, y=2, z=3), dict(x=2, y=3, z=4)),
        title='evt-456'
    )

    mock_adapter = mocker.Mock(spec_set=QueueAdapter)
    mock_adapter.receive_message = mocker.Mock(return_value=iter([reply_message]))
    handler = SearchEventHandler(event=event, queue_adapter=mock_adapter)
    queued_message = handler.handle_event()
    resp = handler.fetch_response(queued_message)
    assert resp == reply_message.body
    mock_adapter.receive_message.assert_called_once()


def test_fetch_response_no_result(mocker: MockerFixture):
    event = SearchApiEvent(
        event_id='evt-123',
        correlation_id='x123',
        query='express'
    )

    mock_adapter = mocker.Mock(spec_set=QueueAdapter)
    mock_adapter.receive_message = mocker.Mock(return_value=iter([]))
    handler = SearchEventHandler(event=event, queue_adapter=mock_adapter)
    handler.doze_factor = 0
    queued_message = handler.handle_event()
    resp = handler.fetch_response(queued_message)
    assert resp == []
