"""
Fetches AWS configuration values needed for an AWS Lambda to access
the Nodezoo Info Service ALB.
"""
import logging
import re

import boto3

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def _create_session(profile: str = None) -> boto3.Session:
    if profile is not None:
        return boto3.Session(profile_name=profile)

    return boto3.Session()


def _fetch_account_id(session: boto3.Session):
    logger.info("Fetching account_id...")
    sts = session.client("sts")
    caller_ident = sts.get_caller_identity()
    return caller_ident["Account"]


def _fetch_info_alb_host(session):
    logger.info("Fetching info_alb_host...")
    elbv2 = session.client("elbv2")
    resp = elbv2.describe_load_balancers()
    rexp = re.compile("k8s-nodezoo-info")
    lb_attrs_ite = filter(lambda lb: rexp.search(lb["LoadBalancerName"]) is not None, resp["LoadBalancers"])
    matched_lbs = tuple(lb_attrs_ite)
    assert len(matched_lbs) == 1
    return matched_lbs[0]["DNSName"]


def _fetch_eks_vpc_id(session):
    logger.info("Fetching eks_vpc_id...")
    ec2 = session.client("ec2")
    resp = ec2.describe_vpcs(
        Filters=[
            dict(
                Name="tag:aws:cloudformation:stack-name",
                Values=["NodezooK8SClusterStack"]
            )
        ]
    )
    matched_vpcs = resp["Vpcs"]
    assert len(matched_vpcs) == 1
    return matched_vpcs[0]["VpcId"]


def _fetch_vpc_private_subnets(session, vpc_id):
    logger.info(f"Fetching vpc_private_subnets for {vpc_id}...")
    ec2 = session.client("ec2")
    resp = ec2.describe_subnets(
        Filters=[
            dict(
                Name="vpc-id",
                Values=[vpc_id]
            ),
            dict(
                Name="tag:aws:cloudformation:stack-name",
                Values=["NodezooK8SClusterStack"]
            ),
            dict(
                Name="tag:aws-cdk:subnet-type",
                Values=["Private"]
            )
        ]
    )
    pvt_subnets = resp["Subnets"]
    assert len(pvt_subnets) >= 1
    return list(map(lambda ps: ps["SubnetId"], pvt_subnets))


def _fetch_security_group_id(session, vpc_id):
    logger.info(f"Fetching security_group_id in {vpc_id}...")
    ec2 = session.client("ec2")
    resp = ec2.describe_security_groups(
        Filters=[
            dict(
                Name="vpc-id",
                Values=[vpc_id]
            ),
            dict(
                Name="tag:ingress.k8s.aws/stack",
                Values=["nodezoo/info-service-ingress"]
            )
        ]
    )
    matching_groups = resp["SecurityGroups"]
    assert len(matching_groups) == 1
    return matching_groups[0]["GroupId"]


def add_attributes(attrs: dict, profile: str = None):
    """
    Modify the attrs, adding all required attributes
    :param attrs: keys added are region, account_id, info_alb_host, vpc_id, vpc_pvt_subnet_ids, security_group_id
    :param profile: AWS profile to be used (optional)
    :return:
    """
    session = _create_session(profile)
    attrs["region"] = session.region_name
    attrs["account_id"] = _fetch_account_id(session)
    attrs["info_alb_host"] = _fetch_info_alb_host(session)
    attrs["vpc_id"] = _fetch_eks_vpc_id(session)
    attrs["vpc_pvt_subnet_ids"] = _fetch_vpc_private_subnets(session, attrs["vpc_id"])
    attrs["security_group_id"] = _fetch_security_group_id(session, attrs["vpc_id"])
