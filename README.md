# Nodezoo API
API gateway. Entry point of the Nodezoo system. Processes API Gateway events and posts a message to SQS. Waits for
response message from reply-queue and returns the message payload. Content type for responses is ``application/json``.

## Usage
The API supports the following request paths.

### GET /info
```
curl https://<aws-api-gw-url>/info?package=<package-name>
```
Response will be the package information from different sources.

#### SQS Message
Structure of the SQS message payload is as follows. The ``package`` request parameter will be the
 ``.body.get`` attribute value.
```
{
  event_id: "string",
  correlation_id: "string",
  body: {
    role: "info",
    type: "cmd",
    cmd: "get",
    get: "string"
  },
  dest: "info-cmd-get"
}
```

### GET /search
```
curl https://<aws-api-gw-url>/search?q=<search-query>
```
Response will package information matching ``search-query``.

#### SQS Message
Structure of the SQS message payload is as follows. The ``q`` request parameter will be the ``.body.query``
attribute value.
```
{
  event_id: "string",
  correlation_id: "string",
  body: {
    role: "search",
    type: "cmd",
    cmd: "search",
    query: "string"
  },
  dest: "search-cmd-search"
}
```

### SQS Message Response format
``.correlation_id`` should be same as ``correlation_id`` from SQS Message received. ``.body`` is the package
information or search results.
```
{
  correlation_id: "string",
  body: "string",
  title?: "string"
}
```

## Development
Local machine or Docker -

### Local machine

#### Poetry
Install [poetry](https://python-poetry.org/)

#### Setup

```shell
cd /path/to/nodezoo-api
poetry install
```

### Docker

If you are a contributor, pull the docker image from the
project registry.

```shell
docker pull registry.gitlab.com/n1470/nodezoo-api
```

Otherwise, you need to build the image and push it your
project registry. Make a note of your relative project
path, for example ``n1470/nodezoo-api``.

```shell
# export PROJECT_PATH='n1470/nodezoo-api'
docker login registry.gitlab.com
docker build --rm \
--build-arg "PROJECT_PATH=$PROJECT_PATH" \
-t registry.gitlab.com/$PROJECT_PATH .
docker push registry.gitlab.com/$PROJECT_PATH
```

#### Usage

All operations can be executed in the docker container
by mounting the current path to /builds/$PROJECT_PATH

```shell
docker run -it \
--rm \
-v"$PWD:/builds/$PROJECT_PATH" \
registry.gitlab.com/$PROJECT_PATH \
make lint
```

## AWS
Check the ALB is ready.
```shell
AWS_PROFILE=cdk make info_alb_ready_check
```

```shell
AWS_PROFILE=cdk make dev_dist dev_deploy
```

## Targets

```shell
make lint
make test
make dist
make deploy
```

## Deploy to AWS

### GitOps
Create and push a git tag beginning with ``aws-rel`` such as ``aws-release-1``. This will create an API gateway and
deploy the Lambda runtime as an API gateway trigger.

### Manual

In order to deploy, three environment variables need to
be defined. Example for docker:

```shell
docker run -it \
--rm \
-v"$PWD:/builds/$PROJECT_PATH" \
--env AWS_ACCESS_KEY_ID='aws access key' \
--env AWS_SECRET_ACCESS_KEY='aws secret key' \
--env AWS_DEFAULT_REGION='us-east-1' \
registry.gitlab.com/$PROJECT_PATH \
make dist deploy
```

### Cloud Formation Output

Following variables can be imported into another Cfn template.

#### SQS Queue URLs

The following are CloudFormation externalized outputs that can be imported into another Cloudformation template.

* ``nodezoo-sqs-info-cmd-get-url`` - package queries are queued here
* ``nodezoo-sqs-info-cmd-get-reply-url`` - package query replies are queued here
* ``nodezoo-sqs-search-cmd-search-url`` - search queries
* ``nodezoo-sqs-search-cmd-search-reply-url`` - search results (replies)


## License
Distributed under the terms of [The Unlicense](https://opensource.org/licenses/unlicense)

## Helpful links for development

- [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:fe3ba9673d15f1521b0eab3155a204aa?https://docs.gitlab.com/ee/user/application_security/sast/)
- [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:fe3ba9673d15f1521b0eab3155a204aa?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
