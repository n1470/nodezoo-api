.phony: test
test:
	@poetry run pytest tests

.phony: watch
watch:
	@ptw --runner "pytest --picked --testmon"

.phony: lint_lite
lint_lite:
	@poetry run flake8

.phony: lint
lint:
	@poetry run pylint --rcfile=pylint.rc \
	nodezoo_api \
	router_lambda \
	api.infrastructure \
	message_queue \
	domain \
	queue_lambda \
	pkg_info_gateway

.phony: clean
clean:
	@set -ev
	@make -f api/Makefile clean
	@make -f pkg_info_gateway/Makefile clean

ifdef ($(AWS_PROFILE))
AWS_CMD := "aws --profile $(AWS_PROFILE)"
else
AWS_CMD := "aws"
endif

dev_dist: clean
	@set -ev
	@make -f api/Makefile dist
	@make -f pkg_info_gateway/Makefile dist
	@if [ -n "$${NODEZOO_INFO_API_URL}" ]; then \
		export NODEZOO_API_APP_ARGS="--info-api-url $${NODEZOO_INFO_API_URL}"; \
	elif [ -n "$${AWS_PROFILE}" ]; then \
		export NODEZOO_API_APP_ARGS="-p $${AWS_PROFILE}"; \
	fi && \
	poetry run cdk synth

.phony: dev_deploy
dev_deploy:
	@poetry run cdk --profile cdk deploy

dist: clean
	@set -ev
	@make -f api/Makefile dist
	@make -f pkg_info_gateway/Makefile dist
	@poetry run cdk synth

.phony: deploy
deploy:
	@poetry run cdk --require-approval=never --ci=true deploy

.phony: info_alb_ready_check
info_alb_ready_check:
	@${AWS_CMD} elbv2 describe-load-balancers | \
    jq -r '.LoadBalancers[] | select(.LoadBalancerName | test("k8s-nodezoo-info")) | {host: .DNSName, state: .State.Code}'

.phony: delete_stack
delete_stack:
	@${AWS_CMD} cloudformation delete-stack --stack-name nodezoo

.phony: ci_image
ci_image:
	@docker build --build-arg PROJECT_PATH=n1470/nodezoo-api -t registry.gitlab.com/n1470/nodezoo-api:latest .

.phony: push_ci_image
push_ci_image:
	@docker push registry.gitlab.com/n1470/nodezoo-api:latest
