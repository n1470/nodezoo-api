"""
AWS Lambda proxy for API Gateway
"""
from nodezoo_api.aws import lambda_adapter


def lambda_handler(event: dict, context):
    """
    AWS Lambda handler.
    :param event:
    :param context: LambdaContext
    :return:
    """
    return lambda_adapter.handle(event=event, context=context)
