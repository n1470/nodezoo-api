"""
Event Handler Factory
"""
from nodezoo_api.core.nodezoo_error import NodezooNotFoundError
from nodezoo_api.domain.info_api_event import InfoApiEvent
from nodezoo_api.domain.search_api_event import SearchApiEvent
from nodezoo_api.handler.info_event_handler import InfoEventHandler
from nodezoo_api.handler.search_event_handler import SearchEventHandler
from nodezoo_api.core.queue_adapter import QueueAdapter
from nodezoo_api.core.app_event import AppEvent
from nodezoo_api.core.abstract_event_handler import AbstractEventHandler


class HandlerFactory:
    """
    Factory for creating event handlers.
    """
    def __init__(self,
                 info_queue_adapter: QueueAdapter,
                 search_queue_adapter: QueueAdapter):
        self._info_queue_adapter = info_queue_adapter
        self._search_queue_adapter = search_queue_adapter

    @property
    def info_queue_adapter(self) -> QueueAdapter:
        """
        Info queue adapter instance
        :return:
        """
        return self._info_queue_adapter

    @property
    def search_queue_adapter(self) -> QueueAdapter:
        """
        Info queue adapter instance
        :return:
        """
        return self._search_queue_adapter

    def create(self, app_event: AppEvent) -> AbstractEventHandler:
        """
        Creates the event handler based on app_event
        :param app_event:
        :return:
        """
        if app_event.request_path == '/info':
            return self._build_info_event_handler(app_event)
        if app_event.request_path == '/search':
            return self._build_search_event_handler(app_event)
        raise NodezooNotFoundError(f"Unknown request path: {app_event.request_path}")

    def _build_search_event_handler(self, app_event):
        api_event = SearchApiEvent(event_id=app_event.request_id,
                                   correlation_id=app_event.correlation_id,
                                   query=app_event.request_params['q'])
        return SearchEventHandler(event=api_event, queue_adapter=self.search_queue_adapter)

    def _build_info_event_handler(self, app_event):
        api_event = InfoApiEvent(event_id=app_event.request_id,
                                 correlation_id=app_event.correlation_id,
                                 resource=app_event.request_path,
                                 data=app_event.request_params['package'])
        return InfoEventHandler(event=api_event, queue_adapter=self.info_queue_adapter)
