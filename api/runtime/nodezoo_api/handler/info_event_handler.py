"""
Handler for Info API events.
"""
from typing import Union

from nodezoo_api.core.abstract_event_handler import AbstractEventHandler
from nodezoo_api.core.nodezoo_error import NodezooNotFoundError
from nodezoo_api.domain.message import Message


class InfoEventHandler(AbstractEventHandler):
    """
    Handler for info events.
    """
    def handle_event(self) -> Message:
        event_attrs = {'role': "info", 'type': "cmd", 'cmd': "get", 'get': self.event.data}
        return self.send_message(event_attrs=event_attrs)

    def transform_response(self, response_body: Union[dict, list, None]) -> Union[dict, list]:
        if response_body is None:
            raise NodezooNotFoundError()
        return response_body
