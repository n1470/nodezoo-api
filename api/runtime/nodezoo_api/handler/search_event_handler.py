"""
Handler for Search API events.
"""
from typing import Union

from nodezoo_api.core.abstract_event_handler import AbstractEventHandler
from nodezoo_api.domain.message import Message


class SearchEventHandler(AbstractEventHandler):
    """
    Handler for search events.
    """
    def handle_event(self) -> Message:
        event_attrs = {'role': "search", 'type': "cmd", 'cmd': "search", 'query': self.event.query}
        return self.send_message(event_attrs=event_attrs)

    def transform_response(self, response_body: Union[dict, list, None]) -> Union[dict, list]:
        next_response = response_body if response_body is not None else []
        return next_response
