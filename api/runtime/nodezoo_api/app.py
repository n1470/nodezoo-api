"""
Main Application
"""
from typing import Union

from nodezoo_api.aws.sqs_adapter import SqsAdapter
from nodezoo_api.core import log_adapter
from nodezoo_api.core.app_event import AppEvent
from nodezoo_api.handler_factory import HandlerFactory
from nodezoo_api.core.abstract_event_handler import AbstractEventHandler

logger = log_adapter.logger()


def process_event(app_event: AppEvent,
                  handler_factory: Union[HandlerFactory, None] = None):
    """
    Hooks the handler logic to the Lambda handler

    :param app_event: Application event
    :param handler_factory: optional
    """
    logger.debug(app_event)
    info_queue_adapter = SqsAdapter(aws_region_code=app_event.aws_region_code,
                                    send_queue_url=app_event.queues.info_queue_url,
                                    receive_queue_url=app_event.queues.info_queue_reply_url)
    search_queue_adapter = SqsAdapter(aws_region_code=app_event.aws_region_code,
                                      send_queue_url=app_event.queues.search_queue_url,
                                      receive_queue_url=app_event.queues.search_queue_reply_url)
    if handler_factory is None:
        handler_factory = HandlerFactory(info_queue_adapter=info_queue_adapter,
                                         search_queue_adapter=search_queue_adapter)
    event_handler: AbstractEventHandler = handler_factory.create(app_event=app_event)
    queued_message = event_handler.handle_event()
    return event_handler.fetch_response(queued_message)
