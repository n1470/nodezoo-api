"""
aws: SQS adapter implementation
"""
import base64
import gzip
import json
import dataclasses
from typing import Union

import boto3
# pylint: disable=unused-import
from aws_xray_sdk.core import xray_recorder  # noqa: F401
from aws_xray_sdk.core import patch_all
from mypy_boto3_sqs.client import SQSClient

from nodezoo_api.core import log_adapter
from nodezoo_api.domain.message import Message
from nodezoo_api.core.nodezoo_error import NodezooError
from nodezoo_api.core.queue_adapter import QueueAdapter

logger = log_adapter.logger()


class SqsAdapter(QueueAdapter):
    """
    SQS adapter implementation
    """
    def __init__(self,
                 aws_region_code: str,
                 send_queue_url: str,
                 receive_queue_url: str,
                 sqs_client: Union[SQSClient, None] = None):
        self._aws_region_code = aws_region_code
        self._send_queue_url = send_queue_url
        self._receive_queue_url = receive_queue_url
        self._sqs_client = sqs_client

    @property
    def sqs_client(self) -> SQSClient:
        """
        :return: SQS client instance
        """
        if self._sqs_client is None:
            patch_all()
            self._sqs_client = boto3.client('sqs', region_name=self._aws_region_code)
        return self._sqs_client

    def post_message(self, queue_name: str, message: Message) -> str:
        """
        Posts the message to SQS
        :param queue_name:
        :param message:
        :return: AWS generated message identifier
        """
        if queue_name != message.dest:
            raise NodezooError(
                f"Destination mismatch. queue_name={queue_name}, message.dest={message.dest}")
        response = self.sqs_client.send_message(QueueUrl=self._send_queue_url,
                                                MessageBody=json.dumps(dataclasses.asdict(message)))
        return response['MessageId']

    def receive_message(self):
        """
        Fetch a message from the queue if immediately available.
        """
        response = self.sqs_client.receive_message(QueueUrl=self._receive_queue_url)
        if 'Messages' not in response or response['Messages'] is None:
            return ()

        collected_messages = []
        for sqs_message in response['Messages']:
            try:
                sqs_payload: str = sqs_message['Body']
                body_dec: bytes = base64.urlsafe_b64decode(sqs_payload)
                body_: bytes = gzip.decompress(body_dec)
                payload_ = json.loads(body_.decode("UTF-8"))
                message = Message(event_id=sqs_message['MessageId'],
                                  correlation_id=payload_['correlation_id'],
                                  body=payload_['body'],
                                  title=payload_.get('title', ''),
                                  receipt_handle=sqs_message['ReceiptHandle'])
            except Exception as exception:  # pylint: disable=broad-except
                logger.error(exception)
                message = Message(event_id=sqs_message['MessageId'],
                                  correlation_id="",
                                  body={},
                                  title="",
                                  receipt_handle=sqs_message['ReceiptHandle'])
            collected_messages.append(message)

        return collected_messages

    def acknowledge_message(self, message_handle: str):
        self.sqs_client.delete_message(QueueUrl=self._receive_queue_url,
                                       ReceiptHandle=message_handle)
