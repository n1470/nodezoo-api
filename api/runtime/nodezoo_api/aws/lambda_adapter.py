"""
AWS Lambda handler
"""
import os
import typing

from nodezoo_api.core import log_adapter
from nodezoo_api.core.nodezoo_error import NodezooException, NodezooError, NodezooNotFoundError
from nodezoo_api.app import process_event as app_process_event
from nodezoo_api.core.app_event import AppEvent
from nodezoo_api.core.queue_set import QueueSet

logger = log_adapter.logger()


def handle(event: dict,
           context,
           process_event=app_process_event,
           proxy_lambda_env: typing.Union[dict, None] = None):
    """
    Handles Lambda event by translating Lambda context and environment to an
    application call.
    :param event:
    :param context:
    :param process_event:
    :return: response from the application.
    """
    request_params = {}
    request_params.update(event['queryStringParameters'])
    request_params.update(event['pathParameters'])
    lambda_env = os.environ if proxy_lambda_env is None else proxy_lambda_env
    queues = QueueSet(search_queue_url=lambda_env['nodezoo_search_queue_url'],
                      search_queue_reply_url=lambda_env['nodezoo_search_reply_queue_url'],
                      info_queue_url=lambda_env['nodezoo_info_queue_url'],
                      info_queue_reply_url=lambda_env['nodezoo_info_reply_queue_url'])
    app_event = AppEvent(request_id=context.aws_request_id,
                         correlation_id=lambda_env['_X_AMZN_TRACE_ID'],
                         aws_region_code=lambda_env['AWS_REGION'],
                         request_path=event['path'],
                         request_params=request_params,
                         queues=queues)
    try:
        res = process_event(app_event=app_event)
        return {'statusCode': 200,
                'headers': {"Content-Type": "application/json"},
                'isBase64Encoded': False,
                'body': res}
    except NodezooNotFoundError:
        return {'statusCode': 404}
    except NodezooError:
        return {'statusCode': 400}
    except NodezooException:
        return {'statusCode': 503}
    except Exception as err:  # pylint: disable=broad-except
        logger.exception(err)
        return {'statusCode': 500}
