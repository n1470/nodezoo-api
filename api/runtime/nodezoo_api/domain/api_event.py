"""
Core dataclass: ApiEvent
"""
from dataclasses import dataclass


@dataclass(frozen=True)
class ApiEvent:
    """
    Api Event raised by an API gateway system or implementation.
    """
    event_id: str
    correlation_id: str
