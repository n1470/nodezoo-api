"""
Core dataclass: Message
"""
from typing import Union

from dataclasses import dataclass
from nodezoo_api.domain.info_api_event import ApiEvent


@dataclass(frozen=True)
class Message(ApiEvent):
    """
    Message send to a queue or topic.
    """
    body: Union[dict, tuple, list]
    dest: Union[str, None] = None
    reply_to: Union[str, None] = None
    title: str = None
    receipt_handle: str = None

    def __post_init__(self):
        if self.dest:
            object.__setattr__(self, "reply_to", f"reply-{self.dest}")
            object.__setattr__(self, "title", self.dest)

    @staticmethod
    def destined_for(role: str, message_type: str, value: str):
        """
        Formats the arguments into a consistent queue name for sending and
        receiving messages.
        :param role:
        :param message_type:
        :param value:
        :return:
        """
        return f"{role}-{message_type}-{value}"
