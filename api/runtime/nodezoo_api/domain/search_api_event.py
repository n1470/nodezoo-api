"""
Core dataclass: SearchApiEvent
"""
from dataclasses import dataclass
from nodezoo_api.domain.api_event import ApiEvent


@dataclass(frozen=True)
class SearchApiEvent(ApiEvent):
    """
    Search event raised by API gateway system
    """
    query: str
