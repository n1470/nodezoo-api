"""
Core dataclass: InfoApiEvent
"""
from dataclasses import dataclass
from nodezoo_api.domain.api_event import ApiEvent


@dataclass(frozen=True)
class InfoApiEvent(ApiEvent):
    """
    Info Api Event raised by API gateway system
    """
    resource: str
    data: str
