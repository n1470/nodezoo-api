"""
Application event
"""
from dataclasses import dataclass
from dataclasses import field
from nodezoo_api.core.queue_set import QueueSet


@dataclass(frozen=True)
class AppEvent:
    """
    Event created by application (app) invoker.
    """
    request_id: str
    correlation_id: str
    request_path: str
    aws_region_code: str
    queues: QueueSet
    request_params: dict = field(default_factory=dict)
