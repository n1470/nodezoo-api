"""
Core class
"""
from dataclasses import dataclass


@dataclass(frozen=True)
class QueueSet:
    """
    Set of queue URLs
    """
    info_queue_url: str
    info_queue_reply_url: str
    search_queue_url: str
    search_queue_reply_url: str
