"""
Common logging adapter
"""
import logging
import os

log_level = os.environ.get("LOG_LEVEL", logging.INFO)


def logger() -> logging.Logger:
    """
    Create a logger instance with the default log level.
    :return: logger instance
    """
    logger_ = logging.getLogger()
    logger_.setLevel(log_level)
    return logger_
