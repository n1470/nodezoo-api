"""
Event handler abstraction
"""
from abc import ABC, abstractmethod
from typing import Union
from time import sleep

from nodezoo_api.domain.api_event import ApiEvent
from nodezoo_api.core.queue_adapter import QueueAdapter
from nodezoo_api.domain.message import Message


class AbstractEventHandler(ABC):
    """
    Event handler abstract class
    """
    DEFAULT_NUM_TRIES = 7
    DOZE_FACTOR = 1.0

    def __init__(self, event: ApiEvent, queue_adapter: QueueAdapter):
        self._event = event
        self._queue_adapter = queue_adapter
        self._num_tries = self.DEFAULT_NUM_TRIES
        self._doze_factor = self.DOZE_FACTOR

    @property
    def event(self) -> ApiEvent:
        """
        API event
        :return:
        """
        return self._event

    @property
    def queue_adapter(self) -> QueueAdapter:
        """
        Queue adapter instance
        :return:
        """
        return self._queue_adapter

    @property
    def num_tries(self):
        """
        Maximum number of attempts to fetch a response from queue.
        :return:
        """
        return self._num_tries

    @num_tries.setter
    def num_tries(self, value: int):
        self._num_tries = value

    @property
    def doze_factor(self):
        """
        Factor to apply to the wait time between attempts. Set to 0(zero) to make immediate
        attempts - this is for testing with a mocked backing service only.
        :return:
        """
        return self._doze_factor

    @doze_factor.setter
    def doze_factor(self, value: float):
        self._doze_factor = value

    @abstractmethod
    def handle_event(self) -> Message:
        """
        Queues a message to react to the event.
        :return: Message instance sent to a queue
        """

    @abstractmethod
    def transform_response(self, response_body: Union[dict, list, None]) -> Union[dict, list]:
        """
        Transform the response from a message queue.
        :param response_body:
        :return: dict or list based on API
        """

    def send_message(self,  event_attrs: dict):
        """
        Post the event to appropriate destination via the queue adapter.
        :param event_attrs:
        :return:
        """
        dest = Message.destined_for(
            role=event_attrs["role"],
            message_type=event_attrs["type"],
            value=event_attrs["cmd"]
        )
        message = Message(
            event_id=self.event.event_id,
            correlation_id=self.event.correlation_id,
            body=event_attrs,
            dest=dest
        )
        self.queue_adapter.post_message(queue_name=dest, message=message)
        return message

    def fetch_response(self, queued_message: Message) -> Union[dict, list]:
        """
        Fetches response to a correlated event
        :param: queued_message A previous queued message
        :return: one or more dictionaries
        """
        try_index = 0
        while try_index < self.num_tries:
            for message in self.queue_adapter.receive_message():
                if message and message.correlation_id == queued_message.correlation_id:
                    self.queue_adapter.acknowledge_message(message.receipt_handle)
                    return self.transform_response(message.body)
            sleep(2**try_index * self.doze_factor)
            try_index += 1
        return self.transform_response(None)

    @classmethod
    def receive_timeout(cls):
        """
        Timeout for receiving a message.
        :return:
        """
        return sum([2**k for k in range(cls.DEFAULT_NUM_TRIES + 1)]) * cls.DOZE_FACTOR
