"""
User defined error / exception
"""


class NodezooError(Exception):
    """
    Base error for Nodezoo API. Error is an unrecoverable situation - resolution requires
    code or configuration change.
    """


class NodezooException(Exception):
    """
    Base exception for Nodezoo API. Exception conditions are recoverable.
    """


class NodezooNotFoundError(NodezooError):
    """
    Error for an entity not found.
    """
