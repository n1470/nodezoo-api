"""
Core interface: QueueAdapter
"""
from abc import ABC, abstractmethod
from nodezoo_api.domain.message import Message


class QueueAdapter(ABC):
    """
    Common signature for a queue adapter implementation.
    """
    @abstractmethod
    def post_message(self, queue_name: str, message: Message):
        """
        Post a message to the queue.
        """

    @abstractmethod
    def receive_message(self):
        """
        Receive a message from a queue assuming poll semantics
        :return:
        """

    @abstractmethod
    def acknowledge_message(self, message_handle: str):
        """
        Acknowledge a message as received. A queue is expected to make the message unavailable.
        :param receipt_handle:
        :return:
        """
