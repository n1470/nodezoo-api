"""
setuptools based module.
"""
from setuptools import setup, find_packages


setup(
    name='nodezoo-api',
    version='0.1.0',
    packages=find_packages(exclude=['test']),
    install_requires=[
        'boto3-stubs[sqs,lambda,apigatewayv2] == 1.20.25',
        'aws-xray-sdk==2.4.3'
    ]
)
