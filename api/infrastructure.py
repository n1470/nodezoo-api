"""
API gateway infrastructure
"""
from os import path
from aws_cdk import aws_lambda, Duration
from aws_cdk import CfnOutput
from aws_cdk import aws_apigateway as api_gw
from aws_cdk.aws_apigateway import StageOptions
from aws_cdk.aws_lambda import Tracing
from aws_cdk.aws_lambda_event_sources import SqsEventSource
from constructs import Construct
from message_queue.infrastructure import QueueBuilder
from pkg_info_gateway.infrastructure import build_trigger_runtime


class ApiBuilder(Construct):
    """
    API gateway and Lambda runtime on AWS
    """
    def __init__(self,
                 scope: Construct,
                 id_: str,
                 *_args,
                 **kwargs) -> None:
        super().__init__(scope, id_)

        self._info_queue_builder: QueueBuilder = kwargs['info_queue_builder']
        self._info_reply_queue_builder: QueueBuilder = kwargs['info_reply_queue_builder']
        self._search_queue_builder: QueueBuilder = kwargs['search_queue_builder']
        self._search_reply_queue_builder: QueueBuilder = kwargs['search_reply_queue_builder']
        self._runtime_dist_dir = kwargs.get('runtime_dist_dir', "runtime_dist")
        self._build(kwargs)

    def _build(self, kwargs):
        runtime_root = path.dirname(__file__)
        dist_path = path.join(runtime_root, self._runtime_dist_dir)
        trigger_runtime = build_trigger_runtime(self,
                                                **kwargs,
                                                env_params=self._lambda_env)
        api_runtime = aws_lambda.Function(self, "router",
                                          runtime=aws_lambda.Runtime.PYTHON_3_9,
                                          handler="router_lambda.lambda_handler",
                                          code=aws_lambda.Code.from_asset(dist_path),
                                          environment=self._lambda_env,
                                          timeout=Duration.minutes(15),
                                          tracing=Tracing.ACTIVE)

        self._info_queue_builder.add_send_permissions(api_runtime)
        self._info_queue_builder.add_receive_permissions(trigger_runtime)
        trigger_runtime.add_event_source(SqsEventSource(self._info_queue_builder.queue))
        self._info_reply_queue_builder.add_send_permissions(trigger_runtime)
        self._info_reply_queue_builder.add_receive_permissions(api_runtime)
        self._search_queue_builder.add_send_permissions(api_runtime)
        self._search_reply_queue_builder.add_receive_permissions(api_runtime)
        api = api_gw.LambdaRestApi(self, 'api-endpoint',
                                   handler=api_runtime,
                                   deploy_options=StageOptions(
                                       tracing_enabled=True
                                   ))
        CfnOutput(self, "api-endpoint-url", value=api.url)

    @property
    def _lambda_env(self):
        return {'nodezoo_info_queue_url': self._info_queue_builder.queue_url,
                'nodezoo_info_reply_queue_url': self._info_reply_queue_builder.queue_url,
                'nodezoo_search_queue_url': self._search_queue_builder.queue_url,
                'nodezoo_search_reply_queue_url': self._search_reply_queue_builder.queue_url}
